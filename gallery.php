<?php
// Database connection parameters
$servername = "localhost";
$username = "root"; // Your MySQL username
$password = ""; // Your MySQL password
$dbname = "js_project"; // Your database name

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

    $sql = "SELECT * FROM wildlife_data";

    // Execute the SQL query
    $result = $conn->query($sql);

    // Check if the query was successful
    if (!$result) {
        die("Error executing query: " . $conn->error);
    }

    // Fetch the data
    $data = $result->fetch_all(MYSQLI_ASSOC);

    // Close connection
    $conn->close();

    // Output data as JSON
    header('Content-Type: application/json');
    echo json_encode($data);
    exit;

?>
