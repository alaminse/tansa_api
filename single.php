<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "js_project";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}
if(isset($_GET['id'])) {
    $id = $conn->real_escape_string($_GET['id']);

    $stmt = $conn->prepare("SELECT * FROM wildlife_data WHERE id = ?");
    $stmt->bind_param("i", $id);
    $stmt->execute();

    $result = $stmt->get_result();
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();

        $stmt_gallery = $conn->prepare("SELECT id, image FROM galleries WHERE wildlife_data_id = ?");
        $stmt_gallery->bind_param("i", $id);
        $stmt_gallery->execute();
        
        $result_gallery = $stmt_gallery->get_result();
        
        $gallery_images = array();
        while ($gallery_row = $result_gallery->fetch_assoc()) {
            $gallery_images[] = array(
                "id" => $gallery_row['id'],
                "image" => $gallery_row['image']
            );
        }        

        $row['gallery_images'] = $gallery_images;
        header('Content-Type: application/json');
        echo json_encode($row);
    } else {
        echo json_encode(array('error' => 'Data not found'));
    }
    $stmt->close();
    $stmt_gallery->close();
} else {
    echo json_encode(array('error' => 'ID parameter is not provided'));
}

$conn->close();
?>
