<?php
    session_start();
    $connection = mysqli_connect("localhost", "root", "", "js_project");

    if (!$connection) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $username = $_POST['username'];
    $password = $_POST['password'];

    $sql = "SELECT * FROM users_info WHERE username='$username'";
    $result = mysqli_query($connection, $sql);

    if (mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);
        if (password_verify($password, $row['password'])) {
            $_SESSION['id'] = $row['id'];
            $_SESSION['role'] = $row['role'];
            $_SESSION['username'] = $username;
            $_SESSION['success_message'] = "Registration successful! Please log in."; 
            header("Location: ./upload/index.html");
        } else {
            $_SESSION['success_message'] = "Credential Not match."; 
            header("Location: login.html");
        }
    } else {
        $_SESSION['success_message'] = "User not found."; 
        header("Location: login.html");
    }

    mysqli_close($connection);
?>
