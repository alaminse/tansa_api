<?php
    session_start();
    $connection = mysqli_connect("localhost", "root", "", "js_project");

    if (!$connection) {
        $_SESSION['error_message'] = "Check Your Database Connection!!!";
        header("Location: register.html");
        // die("Connection failed: " . mysqli_connect_error());
    }

    $name = $_POST['name'];
    $username = $_POST['username'];
    $password = password_hash($_POST['password'], PASSWORD_DEFAULT);

    $sql = "INSERT INTO users_info (name, username, password) VALUES ('$name', '$username', '$password')";
    if (mysqli_query($connection, $sql)) {
        $_SESSION['success_message'] = "Registration successful! Please log in."; 
        header("Location: login.html");
    } else {
        $_SESSION['error_message'] = "Error: " . $sql . "<br>" . mysqli_error($connection);
        header("Location: register.html");
    }

    mysqli_close($connection);
?>
