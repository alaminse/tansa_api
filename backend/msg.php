<?php
    session_start();
    $data = [];

    // Check if there's a success or error message stored in the session
    if (isset($_SESSION['success_message'])) {
        $data['msg'] = $_SESSION['success_message'];
        $data['alert'] = 'bg-success';
        unset($_SESSION['success_message']);
    } elseif (isset($_SESSION['error_message'])) {
        $data['msg'] = $_SESSION['error_message'];
        $data['alert'] = 'bg-danger';
        unset($_SESSION['error_message']);
    } else {
        $data['msg'] = "";
        $data['alert'] = 'bg-info';
    }

    // Check if the user is authenticated (for demonstration purpose)
    $data['auth'] = isset($_SESSION['id']);
    if ($data['auth'] == false) {
        $data['msg'] = 'PLease Login';
        $data['alert'] = 'bg-danger';
    }

    // Encode the data as JSON and echo it
    echo json_encode($data);
?>
