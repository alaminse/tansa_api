<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "js_project";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

session_start();

if (isset($_SESSION['id'])) {
    $user_id = $_SESSION['id'];
    $role = $_SESSION['role'];
} else {
  session_destroy();
  $_SESSION['error_message'] = "Please Login."; 
  header("Location: login.html");
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Get the form data
    $date = $_POST['date'];
    $time = $_POST['time'];
    $lan = $_POST['lan'];
    $lat = $_POST['lat'];
    $wildlife_type = $_POST['wildlife_type'];
    $source = $_POST['source'];
    $copyright_type = $_POST['copyright_type'];
    $file_path = '';
    $location_name = $_POST['location_name'];
    $copyright_by = $_POST['copyright_by'];
    
    // File upload handling for main image
    if(isset($_FILES['file'])){
        $targetDir = "file/";
        $fileType = strtolower(pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION));
        $newFileName = date("Y-m-d_H-i-s") . "." . $fileType;
        $targetFile = $targetDir . $newFileName;
        $uploadOk = 1;

        // Check if file already exists
        if (file_exists($targetFile)) {
            echo "Sorry, file already exists.";
            $uploadOk = 0;
        }

        // Check file size
        if ($_FILES["file"]["size"] > 50000000000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }

        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
        } else {
            if (move_uploaded_file($_FILES["file"]["tmp_name"], $targetFile)) {
                $file_path = $targetFile;
            } else {
                echo "Sorry, there was an error uploading your file.";
                exit();
            }
        }
    } else {
        echo "Error: Main image not uploaded.";
        exit();
    }
    
    $stmt = $conn->prepare("INSERT INTO wildlife_data (user_id, date, time, lan, lat, wildlife_type, source, copyright_type, file_path, location_name, copyright_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
    $stmt->bind_param("sssssssssss", $user_id, $date, $time, $lan, $lat, $wildlife_type, $source, $copyright_type, $file_path, $location_name, $copyright_by);
    $stmt->execute();
    
    // Get the last inserted ID
    $wd_id = $conn->insert_id;

    // Close the statement
    $stmt->close();

    $galleryImages = array();
    if(isset($_FILES['gallery'])){
        // Check if any files were uploaded
        if(is_array($_FILES['gallery']['name'])){
            $galleryCount = count($_FILES['gallery']['name']);
            for($i = 0; $i < $galleryCount; $i++){
                // Process each gallery image file
                $galleryFileType = strtolower(pathinfo($_FILES["gallery"]["name"][$i], PATHINFO_EXTENSION));
                $newGalleryFileName = date("Y-m-d_H-i-s") . "_" . uniqid() . "." . $galleryFileType;
                $targetGalleryFile = $targetDir . $newGalleryFileName;
                $galleryUploadOk = 1;

                // Check file size
                if ($_FILES["gallery"]["size"][$i] > 50000000000) {
                    echo "Sorry, your file is too large.";
                    $galleryUploadOk = 0;
                }

                // Check if $galleryUploadOk is set to 0 by an error
                if ($galleryUploadOk == 0) {
                    echo "Sorry, there was an error uploading your file.";
                } else {
                    // if everything is ok, try to upload file
                    if (move_uploaded_file($_FILES["gallery"]["tmp_name"][$i], $targetGalleryFile)) {
                        $stmt = $conn->prepare("INSERT INTO galleries (image, wildlife_data_id) VALUES (?, ?)");
                        $stmt->bind_param("ss", $targetGalleryFile, $wd_id);
                        $stmt->execute();
                        $stmt->close();
                    } else {
                        echo "Sorry, there was an error uploading your file.";
                    }
                }
            }
        } else {
            echo "No files were uploaded for the gallery.";
        }
    } else {
        echo "Error: Gallery images not uploaded.";
        exit();
    }
    header("Location: index.html");
    exit();
} else {
    echo "Error: Form not submitted.";
}

$conn->close();
?>
