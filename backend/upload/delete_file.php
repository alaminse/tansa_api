<?php
// Database connection parameters
$servername = "localhost";
$username = "root"; // Your MySQL username
$password = ""; // Your MySQL password
$dbname = "js_project"; // Your database name

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}
session_start();

if (isset($_SESSION['id'])) {
    $id = $_SESSION['id'];
    $role = $_SESSION['role'];
} else {
  session_destroy();
  $_SESSION['error_message'] = "Please Login."; 
  header("Location: login.html");
}

// Check if the ID is provided via POST request
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['id'])) {
    $id = $_POST['id']; // ID of the row to delete

    $del = $conn->prepare("SELECT image FROM galleries WHERE id = ?");
    $del->bind_param("i", $id);
    $del->execute();
    $result = $del->get_result();
    $row = $result->fetch_assoc();
    $existingFilePath = $row['image'];

    // Prepare and execute SQL statement to delete the row
    $stmt = $conn->prepare("DELETE FROM galleries WHERE id = ?");
    $stmt->bind_param("i", $id);

    if ($stmt->execute()) {
        // Deletion successful
        if (!empty($existingFilePath) && file_exists($existingFilePath)) {
            unlink($existingFilePath);
        }
        echo json_encode(array("success" => true, "message" => "Row deleted successfully."));
    } else {
        // Error deleting row
        echo json_encode(array("success" => false, "message" => "Error deleting row."));
    }

    // Close prepared statement
    $stmt->close();
} else {
    // ID not provided or invalid request method
    echo json_encode(array("success" => false, "message" => "Invalid request."));
}

// Close database connection
$conn->close();
?>
