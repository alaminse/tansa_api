<?php
    $servername = "localhost";
    $username = "root"; // Your MySQL username
    $password = ""; // Your MySQL password
    $dbname = "js_project"; // Your database name

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    session_start();

    if (isset($_SESSION['id'])) {
        $user_id = $_SESSION['id'];
        $role = $_SESSION['role'];
    } else {
    session_destroy();
    $_SESSION['error_message'] = "Please Login."; 
    header("Location: login.html");
    }

    // Check if the form is submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $id = $_POST['id'];
        $location_name = $_POST['location_name'];
        $date = $_POST['date'];
        $time = $_POST['time'];
        $lan = $_POST['lan'];
        $lat = $_POST['lat'];
        $wildlife_type = $_POST['wildlife_type'];
        $source = $_POST['source'];
        $copyright_type = $_POST['copyright_type'];
        $copyright_by = $_POST['copyright_by'];

        // Check if file is uploaded
        if (!empty($_FILES["file"]["name"])) {
            // File upload handling
            $targetDir = "file/";
            $fileType = strtolower(pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION));
            $newFileName = date("Y-m-d_H-i-s") . "." . $fileType;
            $targetFile = $targetDir . $newFileName;
            $uploadOk = 1;

            // Move the uploaded file to the target directory
            if (!move_uploaded_file($_FILES["file"]["tmp_name"], $targetFile)) {
                // Error uploading file
                echo json_encode(array("success" => false, "message" => "Sorry, there was an error uploading your file."));
                exit;
            }

            $stmt = $conn->prepare("SELECT file_path FROM wildlife_data WHERE id = ?");
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $result = $stmt->get_result();
            $row = $result->fetch_assoc();
            $existingFilePath = $row['file_path'];
            if (!empty($existingFilePath) && file_exists($existingFilePath)) {
                unlink($existingFilePath);
            }

        } else {
            // If no file is uploaded, retain the existing file path in the database
            $stmt = $conn->prepare("SELECT file_path FROM wildlife_data WHERE id = ?");
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $result = $stmt->get_result();
            $row = $result->fetch_assoc();
            $targetFile = $row['file_path'];
        }
        // Update data in the database
        $stmt = $conn->prepare("UPDATE wildlife_data SET user_id=?, date=?, time=?, lan=?, lat=?, wildlife_type=?, source=?, copyright_type=?, file_path=?, location_name=?, copyright_by=? WHERE id=?");
        $stmt->bind_param("sssssssssssi", $user_id, $date, $time, $lan, $lat, $wildlife_type, $source, $copyright_type, $targetFile, $location_name, $copyright_by, $id);

        $galleryImages = array();
        if(isset($_FILES['gallery'])){
            // Check if any files were uploaded
            if(is_array($_FILES['gallery']['name'])){
                $galleryCount = count($_FILES['gallery']['name']);
                for($i = 0; $i < $galleryCount; $i++){
                    // Process each gallery image file
                    $galleryFileType = strtolower(pathinfo($_FILES["gallery"]["name"][$i], PATHINFO_EXTENSION));
                    $newGalleryFileName = date("Y-m-d_H-i-s") . "_" . uniqid() . "." . $galleryFileType;
                    $targetGalleryFile = $targetDir . $newGalleryFileName;
                    $galleryUploadOk = 1;

                    // Check file size
                    if ($_FILES["gallery"]["size"][$i] > 50000000000) {
                        echo "Sorry, your file is too large.";
                        $galleryUploadOk = 0;
                    }

                    // Check if $galleryUploadOk is set to 0 by an error
                    if ($galleryUploadOk == 0) {
                        echo "Sorry, there was an error uploading your file.";
                    } else {
                        // if everything is ok, try to upload file
                        if (move_uploaded_file($_FILES["gallery"]["tmp_name"][$i], $targetGalleryFile)) {
                            $stmt1 = $conn->prepare("INSERT INTO galleries (image, wildlife_data_id) VALUES (?, ?)");
                            $stmt1->bind_param("ss", $targetGalleryFile, $id);
                            $stmt1->execute();
                            $stmt1->close();
                        } else {
                            echo "Sorry, there was an error uploading your file.";
                        }
                    }
                }
            } else {
                echo "No files were uploaded for the gallery.";
            }
        } else {
            echo "Error: Gallery images not uploaded.";
            exit();
        }

        if ($stmt->execute()) {
            // Data updated successfully
            echo "<script>window.location.href = 'edit.html?id=$id';</script>";
        } else {
            // Error updating data
            echo "<script>window.location.href = 'edit.html?id=$id';</script>";
        }
        $conn->close();
    } else {
        // Form not submitted
        echo json_encode(array("success" => false, "message" => "Form not submitted."));
    }
?>
