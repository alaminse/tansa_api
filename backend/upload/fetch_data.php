<?php
// Database connection parameters
$servername = "localhost";
$username = "root"; // Your MySQL username
$password = ""; // Your MySQL password
$dbname = "js_project"; // Your database name

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Start the session
session_start();

// Check if the user is authenticated
if (isset($_SESSION['id'], $_SESSION['role'])) {
    $id = intval($_SESSION['id']); // Sanitize input using intval()

    // Get the user's role
    $role = $_SESSION['role'];

    // Construct the SQL query based on the user's role
    if ($role === 'admin') {
        $sql = "SELECT * FROM wildlife_data";
    } else {
        // Use prepared statement to prevent SQL injection
        $stmt = $conn->prepare("SELECT * FROM wildlife_data WHERE user_id = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();

        // Check if the query was successful
        if (!$result) {
            die("Error executing query: " . $conn->error);
        }

        $sql = "SELECT * FROM wildlife_data WHERE user_id = $id";
    }

    // Execute the SQL query
    $result = $conn->query($sql);

    // Check if the query was successful
    if (!$result) {
        die("Error executing query: " . $conn->error);
    }

    // Fetch the data
    $data = $result->fetch_all(MYSQLI_ASSOC);

    // Close connection
    $conn->close();

    // Output data as JSON
    header('Content-Type: application/json');
    echo json_encode($data);
} else {
    session_destroy();
    $_SESSION['error_message'] = "Please login.";
    header("Location: ../login.html");
    exit;
}
?>
