<?php
// Database connection parameters
$servername = "localhost";
$username = "root"; // Your MySQL username
$password = ""; // Your MySQL password
$dbname = "js_project"; // Your database name

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Fetch data from the database
$sql = "SELECT * FROM tansa LIMIT 1"; // Adjust this query as per your database schema
$result = $conn->query($sql);

// Fetch the first row
$data = $result->fetch_assoc();

// Now $data contains the first row of data, or null if no rows were returned

// Close database connection
$conn->close();

// Return data as JSON response
header('Content-Type: application/json');
echo json_encode($data);
?>
