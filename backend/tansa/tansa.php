<?php
session_start();

// Check if the user is logged in
if (!isset($_SESSION['id'])) {
    // Redirect to the login page if not logged in
    $_SESSION['error_message'] = "Please Login.";
    header("Location: login.html");
    exit(); // Stop further execution
}

// Database connection parameters
$servername = "localhost";
$username = "root"; // Your MySQL username
$password = ""; // Your MySQL password
$dbname = "js_project"; // Your database name

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Check if the form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Retrieve form data
    $end_point = $_POST['end_point'];
    $api_key = $_POST['api_key'];

    // Prepare and execute SQL query to check if the record exists
    $check_sql = "SELECT id FROM tansa LIMIT 1";
    $check_result = $conn->query($check_sql);

    if ($check_result && $check_result->num_rows > 0) {
        // Fetch the id of the existing record
        $row = $check_result->fetch_assoc();
        $record_id = $row['id'];

        // If record exists, update it
        $update_sql = "UPDATE tansa SET end_point = ?, api_key = ? WHERE id = ?";
        $stmt = $conn->prepare($update_sql);
        $stmt->bind_param("ssi", $end_point, $api_key, $record_id);

        // Execute the statement
        if ($stmt->execute()) {
            header("Location: index.html");
            exit(); // Stop further execution
        } else {
            echo "Error updating api key: " . $conn->error;
        }
    } else {
        // If record does not exist, insert it
        $insert_sql = "INSERT INTO tansa (end_point, api_key) VALUES ('$end_point', '$api_key')";
        if ($conn->query($insert_sql) === TRUE) {
            header("Location: index.html");
        } else {
            echo "Error inserting record: " . $conn->error;
        }
    }
}

// Close database connection
$conn->close();
?>