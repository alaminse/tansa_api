-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 05, 2024 at 02:57 PM
-- Server version: 8.0.30
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `js_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` int NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_croatian_ci NOT NULL,
  `wildlife_data_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `image`, `wildlife_data_id`) VALUES
(26, 'file/2024-05-04_17-32-00_66367110c94ac.jpg', 21),
(29, 'file/2024-05-04_17-44-14_663673ee9a1aa.jpg', 20),
(30, 'file/2024-05-04_17-44-14_663673ee9acdf.jpeg', 20),
(31, 'file/2024-05-04_17-44-44_6636740c589ac.jpg', 21),
(32, 'file/2024-05-04_17-44-44_6636740c59b91.jpg', 21);

-- --------------------------------------------------------

--
-- Table structure for table `tansa`
--

CREATE TABLE `tansa` (
  `id` int NOT NULL,
  `end_point` varchar(2555) COLLATE utf8mb4_croatian_ci NOT NULL,
  `api_key` varchar(2555) COLLATE utf8mb4_croatian_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci;

--
-- Dumping data for table `tansa`
--

INSERT INTO `tansa` (`id`, `end_point`, `api_key`, `created_at`) VALUES
(1, 'google.com', 'rerweu450435', '2024-05-04 03:24:59');

-- --------------------------------------------------------

--
-- Table structure for table `users_info`
--

CREATE TABLE `users_info` (
  `id` int NOT NULL,
  `role` varchar(225) COLLATE utf8mb4_general_ci DEFAULT 'user',
  `name` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `username` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users_info`
--

INSERT INTO `users_info` (`id`, `role`, `name`, `username`, `password`) VALUES
(2, 'admin', 'Admin', 'admin@gmail.com', '$2y$10$T3NjWM8R8CNGeDBIFka9qujwKiR/a0/FB9rle75rDSz0Bg1kdwsuC'),
(3, 'user', 'Noman', 'noman@gmail.com', '$2y$10$F5v0z02urcrFG8YZAhbGIug.X/kVOB4pPT7wtatemLF1/14K1OcDS'),
(5, 'user', 'Noman 1', 'noman1@gmail.com', '$2y$10$tK1pRS5wBWiwi94lP7YD6uFpiRaIQePtDUGj9CZynAus2I94XNSW.'),
(6, 'user', 'Noman 2', 'noman2@gmail.com', '$2y$10$dyD/hZtzFnd7SHULIZuFD.O7azjfQFVdoOddgRVEjEjgfDFGeABzK'),
(7, 'user', 'Noman 22', 'noman22@gmail.com', '$2y$10$lsaE5sex7k8M2ZDxjy685eQQMkfEqd6j2Lyho1TkMfnBAuRAFEdpS'),
(8, 'user', 'Noman23', 'noman23@gmail.com', '$2y$10$cqGxo64kKwDtnLEeZz5JH.QN3mXvDLMWDR3bIBBD8Ag4vxGXpj3Ri'),
(10, 'user', 'Noman233', 'noman233@gmail.com', '$2y$10$jOpCpBA33IYP1b68dxF0guf8kef2HHlm38rAnVVIi9PDfCI.n6EYa'),
(11, 'user', 'Noman 43', 'noman43@gmail.com', '$2y$10$8WigCxBjfTyhqrPHquiXZeplbLmmhouzs4dJ1yY3AgNX4BtRNQwo6'),
(12, 'user', 'black', 'nomane3@gmail.com', '$2y$10$UVhrmXpg.a0hWtKQyFZPzOdKsnPseTIjCbG8AX7k0Gz6s4GKOlD.2'),
(13, 'user', 'blackf', 'nomanfe3@gmail.com', '$2y$10$s1bewWLe7vc1h79zFGb62OINSccIRGq1KNpOd0E/EUKBRTjWWk662'),
(14, 'user', 'blackfde', 'nodmanfe3@gmail.com', '$2y$10$EXrcM/UptZsVVVKvi/skM.hHWFMJx6p7TmF.Zmozkb59I9pX4G66a'),
(17, 'user', 'blackfde', 'nodmanfe43@gmail.com', '$2y$10$bR6UOdOyylbflTiquCX4e.xBkoWdkN8HfgroRjoH9Ci8DADqg0F6K'),
(18, 'user', 'Noman rf', 'nomansdf@gmail.com', '$2y$10$fvVduXCKGa5L/9QpcGuiz.aKw5bVRrAAmd6TvrhDeQKh6QO/8FhNC'),
(19, 'user', 'Test', 'test@gmail.com', '$2y$10$SwkptYCCYrzJJuyQ0cMnfOiMpxjEtwZUcXWP9zvYVmuEeMPuelIfm');

-- --------------------------------------------------------

--
-- Table structure for table `wildlife_data`
--

CREATE TABLE `wildlife_data` (
  `id` int UNSIGNED NOT NULL,
  `user_id` int DEFAULT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `lan` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `lat` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `wildlife_type` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `source` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `copyright_type` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `file_path` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `location_name` varchar(225) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `copyright_by` varchar(225) COLLATE utf8mb4_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `wildlife_data`
--

INSERT INTO `wildlife_data` (`id`, `user_id`, `date`, `time`, `lan`, `lat`, `wildlife_type`, `source`, `copyright_type`, `file_path`, `location_name`, `copyright_by`) VALUES
(20, 2, '2024-05-03', '12:06:00', '23.7003', '90.4287', 'mammals', 'google.com', 'open', 'file/2024-05-04_17-44-14.jpg', 'Sybill Ball', 'test'),
(21, 2, '1984-09-08', '16:11:00', 'Voluptate repellendu', 'Omnis architecto fac', 'insects', 'Adipisicing sapiente', 'free', 'file/2024-05-04_17-44-44.webp', 'Doris Wise', 'Quia veritatis minim');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tansa`
--
ALTER TABLE `tansa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_info`
--
ALTER TABLE `users_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `wildlife_data`
--
ALTER TABLE `wildlife_data`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `tansa`
--
ALTER TABLE `tansa`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users_info`
--
ALTER TABLE `users_info`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `wildlife_data`
--
ALTER TABLE `wildlife_data`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
