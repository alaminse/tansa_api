document.addEventListener('DOMContentLoaded', function () {
    fetchData();
    document.querySelectorAll('input[name="filter"]').forEach(radio => {
        radio.addEventListener('change', function () {
            filterData(this.id);
        });
    });

    const searchInput = document.getElementById('searchInput');
    searchInput.addEventListener('input', function () {
        const searchQuery = this.value.trim();
        if (searchQuery !== '') {
            filterData(searchQuery);
        } else {
            filterData('all');
        }
    });
});

function fetchData() {
    // Fetch data from the server
    fetch('gallery.php')
        .then(response => response.json())
        .then(data => {
            renderGallery(data);
        })
        .catch(error => console.error('Error fetching gallery data:', error));
}

function filterData(filter) {
    const cards = document.querySelectorAll('.card');
    const searchInput = document.getElementById('searchInput');

    cards.forEach(card => {
        if (filter === 'all' || card.classList.contains(filter)) {
            card.style.display = 'block';
        } else {
            card.style.display = 'none';
        }
    });

    if (searchInput.value !== "") {
      const val = searchInput.value;
      const radio = document.getElementById(val);
      if (radio && !radio.checked) {
        radio.checked = true;
        radio.dispatchEvent(new Event("change"));
      }
    } else if (filter == 'all') {
        const radio = document.getElementById(filter);
        if (radio && !radio.checked) {
          radio.checked = true;
          radio.dispatchEvent(new Event("change"));
        }
    }
}

function renderGallery(data) {
    const gallery = document.querySelector('.gallery');
    // Clear existing content
    gallery.innerHTML = '';

    // Iterate over each item in the fetched data and create gallery cards
    data.forEach(item => {
        const figure = document.createElement('figure');
        figure.className = `card ${item.wildlife_type}`;
        
        // Create an image element and set its source
        const img = document.createElement('img');
        img.className = 'card__image';
        img.src = `./backend/upload/${item.file_path}`;
        img.alt = '';
        
        // Append the image to the figure element
        figure.appendChild(img);
    
        // Add a click event listener to the figure element
        figure.addEventListener('click', function() {
            // Extract the ID from the item object
            const itemId = item.id; // Assuming the ID property in the item object is named 'id'
    
            // Navigate to single.html with the extracted ID
            window.location.href = `single.html?id=${itemId}`;
        });
    
        // Append the figure element to the gallery
        gallery.appendChild(figure);
    });
    
}
